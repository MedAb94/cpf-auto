/*
|--------------------------------------------------------------------------
| Store > faq > Actions
|--------------------------------------------------------------------------
|
| This file contains the actions property of Auth Vuex Module
|
| You may freely extend this store file if the store file that you will
| be building has similar characteristics.
|
*/
"use strict";
import axios from 'axios'

export default {
    /**
     * Request addProductToCart
     *
     * @param {object} state
     *  : current state of the vuex Object
     *
     *  @param {*} payload
     *    Value(s) passed to be used inside this method.
     *
     * @return {void}
     */
    addProductToCart: (context, product) => {
        //commit("setLoading", true);
        const cartItem = context.state.cart.find(item => item.id === product.id);

        if(!cartItem) {
            context.commit('pushProductToCart', product)
        }else {
            context.commit('incrementProductCart', cartItem)
        }
    },
    fetchExtra: (context, product) => {
        //commit("setLoading", true);
        axios.get("https://still-garden-34311.herokuapp.com/products-extra").then(res => {
            context.commit('saveExtra', res.data.data)
        })
    },
    completOrder: (context, product) => {
        //commit("setLoading", true);
        axios.post("https://still-garden-34311.herokuapp.com/client/order", product).then(res => {
            context.commit('completOrder', res.data.data)
        })
    },

    saveContact: (context, contact) => {
        //commit("setLoading", true);
        axios.post("https://still-garden-34311.herokuapp.com/contacts", contact);
    },
    saveEmail: (context, contact) => {
        //commit("setLoading", true);
        axios.post("https://still-garden-34311.herokuapp.com/newsletters", contact);
    },
};
