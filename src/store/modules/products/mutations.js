/*
|--------------------------------------------------------------------------
| Store > Products > Mutations
|--------------------------------------------------------------------------
|
| This file contains the mutations property of Auth Vuex Module
|
| You may freely extend this store file if the store file that you will
| be building has similar characteristics.
|
*/
import sumBy from 'lodash/sumBy';

export default {

    /**
     * Set faq object state
     *
     * @param {object} state
     * :current state of the vuex Object
     *
     *  @param {*} payload
     *    Value(s) passed to be used inside this method.
     *
     * @return {object}
     */
    setProducts: (state, payload) => {
        state.products = payload.data.data;
    },

    pushProductToCart(state, payload) {
        let data = {
            id:payload.id,
            product: payload,
            total: payload.price,
            quantity: 1,
            extra: {}
        }

        state.cart.push(data);

        console.log(state);
        this.commit('products/saveCart');
        this.commit('products/saveCartSum');
    },

    incrementProductCart(state, payload) {
        const cartItem = state.cart.find(item => item.id === payload.id);
        cartItem.quantity ++;
        cartItem.total += cartItem.product.price;

        this.commit('products/saveCart');
        this.commit('products/saveCartSum');
        console.log(state.cart)
    },
    decrementProductCart(state, payload) {
        const cartItem = state.cart.find(item => item.id === payload.id);
        if(cartItem.quantity > 1){
            cartItem.quantity --;
            cartItem.total -= cartItem.product.price;
        } 

        this.commit('products/saveCart');
        this.commit('products/saveCartSum');
    },

    saveCart(state) {
        window.localStorage.setItem('cart', JSON.stringify(state.cart));
    },

    saveCartSum(state) {
        state.cartTotal = sumBy(state.cart, function(p) {
            return parseFloat(p.product.price * p.quantity);
        })

        window.localStorage.setItem('cartTotal', state.cartTotal);
    },
    removeFromCart(state, item) {
        let index = state.cart.indexOf(item);
    
        if (index > -1) {
            let product = state.cart[index];
            state.cart.splice(index, 1);
        }
        this.commit('products/saveCart');
        this.commit('products/saveCartSum');
    },

    completOrder(state, item) {
        state.cart = [];
        state.cartTotal = 0;
        this.commit('products/saveCart');
        this.commit('products/saveCartSum');
    },

    saveExtra(state, payload) {
        state.extra = payload;
    },

    insertQuantityExtra(state, payload) {
        const cartItem = state.cart.find(item => item.id === payload.product.id);

        if(!cartItem) {
            payload["id"] = payload.product.id,
            payload["total"] = payload.product.price,
            state.cart.push(payload);
        }else {
            this.commit('products/incrementProductCart', cartItem)
        }

        this.commit('products/saveCart');
        this.commit('products/saveCartSum');
    },

}