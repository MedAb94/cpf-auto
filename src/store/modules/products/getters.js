/*
|--------------------------------------------------------------------------
| Store > Products > getters
|--------------------------------------------------------------------------
|
| This file contains the getters property of Auth Vuex Module
|
| You may freely extend this store file if the store file that you will
| be building has similar characteristics.
|
*/
'use strict';

export default {
  /**
   * get faq object state
   *
   * @param {object} state
   * :current state of the vuex Object
   *
   * @return {object}
   */
  getCart: (state) => state.cart,
  getProducts: (state) => state.products,
  cartTotal: (state) => state.cartTotal,
  getExtra: (state) => state.extra,
}
