/*
|--------------------------------------------------------------------------
| Store > faq
|--------------------------------------------------------------------------
|
| A Class file that manages all the properties and abilities in relation
| to Bookings.
|
*/
'use strict';

import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'


export default {
    namespaced: true,
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions,
} // End of export default
