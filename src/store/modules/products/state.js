/*
|--------------------------------------------------------------------------
| Store > faq > State
|--------------------------------------------------------------------------
|
| This file contains the state property of Auth Vuex Module
|
| You may freely extend this store file if the store file that you will
| be building has similar characteristics.
|
*/
'use strict';
let cart = window.localStorage.getItem('cart');
let cartTotal = window.localStorage.getItem('cartTotal');

export default {
  /**
   * Array of faqs data
   * @type {array|null}
   */

  products:[],

  /**
   * Array of faqs data
   * @type {array|null}
   */

   cart: cart ? JSON.parse(cart) : [],

   cartTotal: cartTotal ? parseFloat(cartTotal) : 0,

   extra: []
}
